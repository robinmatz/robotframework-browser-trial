REGISTER_TITLE = "Demo Web Shop. Register"

# Your Personal Details
REGISTER_GENDER_MALE = "#gender-male"
REGISTER_GENDER_FEMALE = "#gender-female"
REGISTER_FIRST_NAME = "#FirstName"
REGISTER_LAST_NAME = "#LastName"
REGISTER_EMAIL = "#Email"

# Your Password
REGISTER_PASSWORD = "#Password"
REGISTER_CONFIRM_PASSWORD = "#ConfirmPassword"

REGISTER_REGISTER = "#register-button"

# Register
REGISTER_RESULT = ".result"
REGISTER_CONTINUE = ".register-continue-button"
