LOGIN_PAGE_TITLE = "Demo Web Shop. Login"

# New Customer
LOGIN_PAGE_NEW_CUSTOMER = ".register-button"

# Returning Customer
LOGIN_PAGE_EMAIL = "#Email"
LOGIN_PAGE_PASSWORD = "#Password"
LOGIN_PAGE_REMEMBER_ME = "#RememberMe"
LOGIN_PAGE_FORGOT_PASSWORD = "[href='/passwordrecovery']"
LOGIN_PAGE_LOG_IN = ".login-button"