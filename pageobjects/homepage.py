HOMEPAGE_URL = "http://demowebshop.tricentis.com/"

# Top level links
HOMEPAGE_REGISTER = ".ico-register"
HOMEPAGE_ACCOUNT = ".header-links .account"
HOMEPAGE_LOGIN = ".ico-login"
HOMEPAGE_LOG_OUT = ".ico-logout"
HOMEPAGE_SHOPPING_CART = "#topcartlink .cart-label"
HOMEPAGE_WHISHLIST = ".header-links .ico-wishlist"

HOMEPAGE_SEARCH_STORE = "#small-searchterms"
HOMEPAGE_SEARCH = ".search-box-button"

# Product category navbar
HOMEPAGE_NAV_BOOKS = ".top-menu [href='/books']"
HOMEPAGE_NAV_COMPUTERS = ".top-menu [href='/computers'].mob-top-menu [href='/computers']"
HOMEPAGE_NAV_ELECTRONICS = ".top-menu [href='/electronics']"
HOMEPAGE_NAV_APPAREL_AND_SHOES = ".top-menu [href='/apparel-shoes']"
HOMEPAGE_NAV_DIGITAL_DOWNLOADS = ".top-menu [href='/digital-downloads']"
HOMEPAGE_NAV_JEWELRY = ".top-menu [href='/jewelry']"
HOMEPAGE_NAV_GIFT_CARDS = ".top-menu [href='/gift-cards']"

# Left side menu
## Categories
HOMEPAGE_CATEGORIES_BOOKS = ".block-category-navigation [href='/books']"
HOMEPAGE_CATEGORIES_COMPUTERS = ".block-category-navigation [href='/computers']"
HOMEPAGE_CATEGORIES_ELECTRONICS = ".block-category-navigation [href='/electronics']"
HOMEPAGE_CATEGORIES_APPAREL_AND_SHOES = ".block-category-navigation [href='/apparel-shoes']"
HOMEPAGE_CATEGORIES_DIGITAL_DOWNLOAD = ".block-category-navigation [href='/digital-downloads']"
HOMEPAGE_CATEGORIES_JEWELRY = ".block-category-navigation [href='/jewelry']"
HOMEPAGE_CATEGORIES_GIFT_CARDS = ".block-category-navigation [href='/gift-cards']"
## Manufacturers
HOMEPAGE_MANUFACTURERS_TRICENTIS = "[href='/tricentis']"

# Right side menu
## Newsletter
HOMEPAGE_NEWSLETTER_EMAIL = "#newsletter-email"
HOMEPAGE_NEWSLETTER_SUBSCRIBE = "#newsletter-subscribe-button"
## Recently viewed products

## Community poll
HOMEPAGE_POLL_EXCELLENT = "[value='1']"
HOMEPAGE_POLL_GOOD = "[value='2']"
HOMEPAGE_POLL_POOR = "[value='3']"
HOMEPAGE_POLL_BAD = "[value='4']"
HOMEPAGE_POLL_VOTE = ".vote-poll-button"

# Footer
## Information
HOMEPAGE_INFORMATION_SITEMAP = "[href='/sitemap']"
HOMEPAGE_INFORMATION_SHIPPING_AND_RETURNS = "[href='/shipping-returns']"
HOMEPAGE_INFORMATION_PRIVACY_NOTICE = "[href='/privacy-policy']"
HOMEPAGE_INFORMATION_CONDITIONS_OF_USE = "[href='/conditions-of-use']"
HOMEPAGE_INFORMATION_ABOUT_US = "[href='/about-us']"
HOMEPAGE_INFORMATION_CONTACT_US = "[href='/contactus']"
## Customer service
HOMEPAGE_CUSTOMER_SERVICE_SEARCH = "[href='/search']"
HOMEPAGE_CUSTOMER_SERVICE_NEWS = "[href='/news']"
HOMEPAGE_CUSTOMER_SERVICE_BLOG = "[href='/blog']"
HOMEPAGE_CUSTOMER_SERVICE_RECENTLY_VIEWED_PRODUCTS = "[href='/recentlyviewedproducts']"
HOMEPAGE_CUSTOMER_SERVICE_COMPARE_PRODUCTS_LIST = "[href='/compareproducts']"
HOMEPAGE_CUSTOMER_SERVICE_NEW_PRODUCTS = "[href='/newproducts']"
## My account
HOMEPAGE_MY_ACCOUNT_MY_ACCOUNT = ".account"
HOMEPAGE_MY_ACCOUNT_ORDERS = "[href='/customer/orders']"
HOMEPAGE_MY_ACCOUNT_ADDRESSES = "[href='/customer/addresses']"
HOMEPAGE_MY_ACCOUNT_SHOPPING_CART = ".my-account .ico-cart"
HOMEPAGE_MY_ACCOUNT_WHISHLIST = ".my-account .ico-wishlist"
## Follow Us
HOMEPAGE_FOLLOW_US_FACEBOOK = "[href='http://www.facebook.com/nopCommerce']"
HOMEPAGE_FOLLOW_US_TWITTER = "[href='https://twitter.com/nopCommerce']"
HOMEPAGE_FOLLOW_US_RSS = "[href='/news/rss/1']"
HOMEPAGE_FOLLOW_US_YOUTUBE = "[href='http://www.youtube.com/user/nopCommerce']"
HOMEPAGE_FOLLOW_US_GOOGLE_PLUS = "[href='https://plus.google.com/+nopcommerce']"

# Bottom of page
HOMEPAGE_BOTTOM_POWERED_BY = ".footer-poweredby"
HOMEPAGE_BOTTOM_COPYRIGHT = ".footer-disclaimer"
