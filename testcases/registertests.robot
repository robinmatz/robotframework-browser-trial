*** Settings ***
Documentation     This test suite contains registration tests.
Resource          ../keywords/settings.resource

*** Test Cases ***
Valid Registration
    [Tags]    Smoke
    [Documentation]    Tests the valid registration.
    Generate Testdata
    Open Browser To Homepage
    Go To Register Page
    Enter Registration Details    ${FIRST_NAME}    ${LAST_NAME}    ${EMAIL}    ${PASSWORD}    ${PASSWORD}
    Registration Should Be Successful

*** Keywords ***
Generate Testdata
    [Documentation]    Generates test data on the fly.
    [Arguments]    ${gender}=female
    IF    "${gender}" == "female"
        ${FIRST_NAME}    First Name Female
        ${LAST_NAME}    Last Name Female
    END
    IF    "${gender}" == "male"
        ${FIRST_NAME}    First Name Male
        ${LAST_NAME}    Last Name Male
    END
    ${EMAIL}    Email
    ${PASSWORD}    Password
    ${DIFFERENT_PASSWORD}    Password
    Set Test Variable    ${FIRST_NAME}
    Set Test Variable    ${LAST_NAME}
    Set Test Variable    ${EMAIL}
    Set Test Variable    ${PASSWORD}
    Set Test Variable    ${DIFFERENT_PASSWORD}
