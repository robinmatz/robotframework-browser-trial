*** Settings ***
Resource        ../keywords/settings.resource

*** Variables ***
${USERNAME}        crypt:OiXHPrFY1qKw4p52+f47ZoCinaLUl3scVwrB4KxIE2H7rMgv1VgERHrXfl3QA1wAhq+EtF893H06vWq/CBrG
${PASSWORD}        crypt:dAbcL2KsuklGtvx+XISZQK8ZeQt4N/4dOxAVVEXABz0Hb5tMdowy9LrZSKV0ThD3ukwrBrBjRNblOQ==        

*** Test Cases ***
LocalStorage
    [Setup]    Initialize Testcase
    ${local_storage}    Execute Javascript    window.localStorage
    Log    ${local_storage}

Place Order And Payment Details
    [Setup]    Initialize Testcase
    Go To Apparel And Shoes
    Select Product By Name    50's Rockabilly Polka Dot Top JR Plus Size

*** Keywords ***
Initialize Testcase
    Open Browser To Homepage
    Go To Login Page
    Valid Login    ${USERNAME}    ${PASSWORD}