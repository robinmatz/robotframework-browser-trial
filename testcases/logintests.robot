*** Settings ***
Documentation     Tests concerning login.
Resource          ../keywords/settings.resource

*** Variables ***
${USER}           crypt:ovy3qX0/MuCkKmEAy2ysZNF3Fe+iglI14/RRyUTSNhufZ3uzkyLVKD847kdJbFlcI6rFyD51Xo2EbO3Fx/U3
${PASSWORD}       crypt:wL90xbJ+XRb8nOj6mV/7rorMVk4C7swcSPwk6dPt93CysWgtDENkfzTY4+NHKJBWEmQrzk4GNN7LZQ==

*** Test Cases ***
Valid Login
    [Tags]    Smoke
    [Documentation]    Tests login with valid credentials
    Open Browser To Homepage
    Go To Login Page
    Valid Login    ${USER}    ${PASSWORD}
