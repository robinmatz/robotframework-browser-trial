FROM python:3.9-bullseye

ENV WORK_DIR /opt/robotframework

WORKDIR ${WORK_DIR}

ADD requirements.txt .

RUN apt-get update && apt-get upgrade \
    # install nodejs and npm
    && curl -fsSL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install -y nodejs \
    && node --version \
    && npm --version \
    # install host dependencies for browser library
    && apt-get install -y \
    libglib2.0-0 \
    libnss3 \
    libnspr4 \
    libatk1.0-0 \
    libatk-bridge2.0-0 \
    libcups2 \
    libglib2.0-0 \
    libdrm2 \
    libdbus-1-3 \
    libxcb1 \
    libxkbcommon0 \
    libx11-6 \
    libxcomposite1 \
    libxdamage1 \
    libxext6 \
    libxfixes3 \
    libxrandr2 \
    libgbm1 \
    libpango-1.0-0 \
    libcairo2 \
    libasound2 \
    libatspi2.0-0

ENV VIRTUAL_ENV ${WORK_DIR}/.venv
RUN python -m venv ${VIRTUAL_ENV}
ENV PATH="${VIRTUAL_ENV}/bin:$PATH"

RUN python3 -m pip install --upgrade --no-cache-dir \
    pip setuptools wheel \
    && pip install --no-cache-dir -r requirements.txt \
    && rfbrowser init
